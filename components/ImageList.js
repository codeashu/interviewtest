import React from 'react';
import { 
    Text, 
    FlatList, 
    View, 
    StyleSheet,
    Image,
    Dimensions,
    TouchableOpacity ,
    Animated,
    ScrollView
} from 'react-native';
// import { ScrollView } from 'react-native-gesture-handler';
var {height, width} = Dimensions.get('window');

export class ImageList extends React.Component {
  render() {
      const {
        imageArray,
        animation,
        refss,
        checkAnim
      } = this.props;
    return (
        <ScrollView 
            scrollEnabled={!checkAnim}
            scrollEventThrottle={16}
            onMomentumScrollEnd={event => { 
                this.props.onScrollEnd(event.nativeEvent.contentOffset.y)
            }}
        >
            <View style={{paddingTop:15}}>
            {imageArray.map((item,index)=>{
                return(
                    <View key={index} style={{flex:1}}>
                        <TouchableOpacity
                                ref={refss}
                                onPress={()=>{this.props.onCardClick(item, height*0.15,index)}}>
                                <View  style={styles.listContainer}>
                                    <View style={styles.subConatiner}>
                                        <Image
                                            
                                            style={{width:width * 0.38,height:height*0.15}}
                                            source={item.imageUrl}
                                        />
                                        <View
                                            style={[{flex:1, marginLeft:5,justifyContent:'center',alignContent:'center',paddingTop:1,paddingBottom:3}]}>
                                            <Text style={{marginBottom:10, fontSize:14,fontWeight:'700'}}>
                                                {item.imageTitle}
                                            </Text>
                                            <Text style={{fontSize:12, color:'black'}} >
                                                {item.des}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                        </TouchableOpacity>
                    </View>
                    )
                })}
            </View>
        </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
    listContainer: {
    //   flex:1,
      height:height*0.16,
      backgroundColor:'white',
      marginBottom:10,
    //   height:height * 0.2
    },
    subConatiner:{
        // padding:1,
        // paddingTop:3,
        // paddingBottom:3,
        flexDirection:'row',
        justifyContent:'center',
        alignContent:'center',
        borderWidth:1.3,
        borderColor:'#ccc',
        // height:height * 0.2,
        // elevation:1,
        shadowOpacity: 0.3,
        shadowRadius: 1,
        shadowColor: '#000',
        // marginBottom:5,
        shadowOffset: {
            width: 0, height: 4,
        },
        borderRadius:5
        // marginTop:-10
    },
    
  });