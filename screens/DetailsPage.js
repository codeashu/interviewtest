import React from 'react';
import { 
  ScrollView, 
  StyleSheet, 
  View, 
  Text, 
  Dimensions,
  Image,
  Animated,
 } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
var {height, width} = Dimensions.get('window');


export default class DetailsPage extends React.Component {
  state = {
    animation : new Animated.Value(0)
  }
  static navigationOptions = {
    title: 'DetailsPage',
  };
  componentWillMount(){
    
    // Animated.timing(                  // Animate over time
    //   this.state.animation,            // The animated value to drive
    //   {
    //     toValue: 1,                   // Animate to opacity: 1 (opaque)
    //     duration: 800,              // Make it take a while
    //     useNativeDriver: true
    //   }
    // ).start(()=>{
    //   // this.props.navigation.navigate('Details',{
    //   //   item:item
    //   // })
    // });           // The animated value to drive
  }
  render() {
    const transform = [{
      translateY: this.state.animation.interpolate({
        inputRange: [0, 1],
        outputRange: [100, 0]
      })
    }]
    const opacity = this.state.animation
    const { item } = this.props.navigation.state.params;
    return (
        <View style={[StyleSheet.absoluteFill, styles.container]}>
          <Image
            source={item.imageUrl}
            style={{
              width: width ,
              height: 300,
            }}
          />
          <Animated.View style={[styles.detailBody]}>
            <Text style={styles.title}>
              {item.imageTitle}
            </Text>
            <Text style={styles.description}>
              {item.des}
            </Text>
          </Animated.View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 22,
    fontWeight: '700',
    lineHeight: 50
  },
  description: {
    fontSize: 14,
  },
  detailBody:{
    flex:1,
    padding:15
  }
});
