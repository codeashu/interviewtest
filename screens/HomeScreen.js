import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Animated,
  Dimensions,
  UIManager,
  findNodeHandle
} from 'react-native';
import { WebBrowser } from 'expo';
import { ImageList } from '../components/ImageList';
import { MonoText } from '../components/StyledText';
import { imageArray } from '../json/index';
var {height, width} = Dimensions.get('window');



export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Choose item',
  };
  state = {
    animation: new Animated.Value(0),
    item:null,
    layout:null,
    heights:null,
    i:null,
    first:false,
    yScroll:null,
    anim:false
  }
  onCardClick = (item, evt,i) => {
    if(!this.state.anim){
      const {yScroll} = this.state;
      const ik = i == 0 || i == 1 ? 1 : i ;
      UIManager.measure(findNodeHandle(this.view),(x,y,width,height,pageX,pageY)=>{
        const yZ = i == 0 ? 15 :height * ik + 15.3 ;
        const scrollPresent = yScroll !== null ? yZ - yScroll : yZ;
        this.setState({item:item,heights:scrollPresent,i,first:true, anim:true},()=>{
          Animated.timing(                  // Animate over time
            this.state.animation,            // The animated value to drive
            {
              toValue: 1,                   // Animate to opacity: 1 (opaque)
              duration: 1000,              // Make it take a while
            }
          ).start(()=>{
            this.props.navigation.navigate('Details',{
              item:item
            })
          });
        })
      })
    }
  }
  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        Animated.timing(                  // Animate over time
          this.state.animation,            // The animated value to drive
          {
            toValue: 0,                   // Animate to opacity: 1 (opaque)
            duration: 1000,              // Make it take a while
          }
        ).start(()=>{
          if(this.state.first){
            this.setState({
              item:null,
              anim:false
            })
          }
        });
      }
    );
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }
  render() {
    const {
      animation,
      layout,
      heights,i
    } = this.state;
    const opacity = this.state.animation.interpolate({
      inputRange: [0,0.55, 1],
      outputRange: [1,0.55, 0]
    })
    const textOpacity = this.state.animation.interpolate({
      inputRange:[0,0.55,1],
      outputRange:[0,0.55,1]
    })
    const imageMove = {
        position: 'absolute',
        width: animation.interpolate({
            inputRange:[0,1],
            outputRange:[width * 0.38,width]
        }),
        height: animation.interpolate({
            inputRange:[0,1],
            outputRange:[height*0.15,300]
        }),
        left: animation.interpolate({
            inputRange:[0,1],
            outputRange:[10,0]
        }),
        top:animation.interpolate({
            inputRange:[0,1],
            outputRange:[heights,0]
        }),
        

    }
    
    const textMove = {
      position: 'absolute',
      padding:15,
      flex:1,
      // left:160,
      left:animation.interpolate({
        inputRange:[0,1],
        outputRange:[150,0]
      }),
      top:animation.interpolate({
          inputRange:[0,1],
          outputRange:[heights,310]
      }),
      padding:animation.interpolate({
        inputRange:[0,1],
        outputRange:[0,15]
      }),
      opacity:animation.interpolate({
        inputRange:[0,0.2,1],
        outputRange:[0.1,0.2,1]
      }),
      width:animation.interpolate({
        inputRange:[0,1],
        outputRange:[width*0.5,width]
    })

  }
  const des = {
    fontSize:animation.interpolate({
      inputRange:[0,1],
      outputRange:[12,14]
    }),
  }
  const textSize = {
    marginBottom:10,
    fontWeight:'700',
    fontSize:animation.interpolate({
      inputRange:[0,1],
      outputRange:[14,22]
    }),
    
  }
    const transform = [{
      translateY: animation.interpolate({
        inputRange: [0, 1],
        outputRange: [400, 0]
      })
    }]
    
    return (
      <View style={{flex:1}}>
        <Animated.View style={[styles.mainContainer,{opacity:opacity}]}>
          <ImageList
            checkAnim={this.state.anim}
            refss={ref => this.view = ref}
            animation={this.state.animation}
            onCardClick={(item, evt,i)=>{this.onCardClick(item, evt,i)}}
            imageArray={imageArray}
            onScrollEnd={(yScroll)=>{this.setState({yScroll})}}
          />
        </Animated.View>
        {this.state.item !== null ?
          <Animated.Image
              style={[imageMove]}
              source={this.state.item.imageUrl}
          />
        :null}
        {this.state.item !== null ?
          <Animated.View style={textMove}>
            <Animated.Text style={textSize}>
              {this.state.item.imageTitle}
            </Animated.Text>
            <Animated.Text style={des}>
              {this.state.item.des}
            </Animated.Text>
          </Animated.View>
        :null}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  mainContainer: {
    flex:1,
    paddingLeft:10,
    paddingRight:10,
  },
  title: {
    fontSize: 22,
    fontWeight: '600',
    lineHeight: 50
  },
  description: {
    fontSize: 14,
  },
  detailBody:{
    flex:1,
    padding:15
  }
});
