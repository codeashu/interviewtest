import React from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import {
  Animated,
  Easing
} from 'react-native'
import HomeScreen from '../screens/HomeScreen';
import DetailsPage from '../screens/DetailsPage';

const transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 750,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {      
      const { position, scene } = sceneProps

      const thisSceneIndex = scene.index

      const opacity = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [0, 1],
      })

      return { opacity } 
    },
  }}

export default createAppContainer(createStackNavigator({
  Home: HomeScreen,
  Details: DetailsPage,
},
{
  transitionConfig
}
));